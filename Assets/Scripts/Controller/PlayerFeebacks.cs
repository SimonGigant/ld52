﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFeebacks : MonoBehaviour
{
    public GameObject stepDust;

    private readonly float dustDelay = 0.5f;
    private float _dustCount = 0f;


    public void SpawnDust()
    {
        if (_dustCount < dustDelay)
            return;
        GameObject.Instantiate(stepDust, transform.position, Quaternion.identity);
        _dustCount = Random.Range(0f, 0.1f);
    }

    private void Update()
    {
        _dustCount += Time.deltaTime;
    }
}
