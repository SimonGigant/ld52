﻿using System;
using System.Collections;
using System.Collections.Generic;
using GigSpeech;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SokobanPlayer : SokobanEntity
{
    //TODO: replace
    private static SokobanPlayer _instance;
    public static SokobanPlayer Instance
    {
        get
        {
            return _instance;
        }
    }

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
    }


    public override bool IsPlayer()
    {
        return true;
    }


    //Cache:
    private Rigidbody2D _rb;
    private InputActionAsset _actions;
    private Animator _animator;
    private PlayerFeebacks _feedback;
    public int _movementCached;

    //Parameters:

    //Changing values:
    private Vector2 _moveDirection = Vector2.zero;
    public bool canPlay = true;

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
        _actions = GetComponent<PlayerInput>().actions;
        _animator = GetComponent<Animator>();
        _feedback = GetComponent<PlayerFeebacks>();
        SubscribeActionEvents();

        if (ParallaxCamera.Instance)
        {
            ParallaxCamera.Instance.follow = transform;
        }
    }

    private void SubscribeActionEvents()
    {
        _actions["Move"].performed += OnMoveInput;
        _actions["Move"].canceled += OnMoveInput;
        _actions["Interact"].started += OnInteractButton;
        _actions["ReloadScene"].started += OnReloadSceneInput;
    }

    void UnsubscribeActionEvents()
    {
        _actions["Move"].performed -= OnMoveInput;
        _actions["Move"].canceled -= OnMoveInput;
        _actions["Interact"].started -= OnInteractButton;
        _actions["ReloadScene"].started -= OnReloadSceneInput;
    }

    protected override void OnMovementComplete()
    {
        base.OnMovementComplete();
        if (_movementCached > 0)
        {
            Tuple<int, int> direction = new Tuple<int, int>(Mathf.FloorToInt(_moveDirection.x), Mathf.FloorToInt(_moveDirection.y));
            TryMove(direction);
            --_movementCached;
        }
    }
    
    private void OnMoveInput(InputAction.CallbackContext context)
    {
        if (!GameflowManager.Instance.playerCanInteract)
            return;
        if (context.ReadValue<Vector2>().magnitude > 0)
        {
            _moveDirection = context.ReadValue<Vector2>();
            Tuple<int, int> direction = new Tuple<int, int>(Mathf.FloorToInt(_moveDirection.x), Mathf.FloorToInt(_moveDirection.y));
            if (!TryMove(direction))
            {
                ++_movementCached;
            }
            else
            {
                _movementCached = 0;
            }
        }
    }

    private void OnReloadSceneInput(InputAction.CallbackContext context)
    {
        GameflowManager.Instance.ReloadLevel();
    }

    private void OnInteractButton(InputAction.CallbackContext context)
    {
        if (GigSpeechManager.Instance != null)
        {
            GigSpeechManager.Instance.Pass();
        }
    }

    private void FixedUpdate()
    {
        if (!canPlay)
            return;
    }

    private void OnDestroy()
    {
        UnsubscribeActionEvents();
    }
}
