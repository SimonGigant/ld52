using UnityEngine;

namespace GigSpeech
{
    public class GigSpeechConversation : MonoBehaviour
    {
        public GigSpeechConversationData conversationData;

        //Starts with dialogue tagged with id 0
        [SerializeField] private bool startWithADialogue = true;

        public void StartConversation()
        {
            if (startWithADialogue)
            {
                if (Trigger(0))
                    return;
            }
            
            //TODO: Print Options Menu
        }
        
        public bool Trigger(int id)
        {
            if(conversationData == null || conversationData.options == null)
                return false;
            foreach (GigSpeechConversationOptionBase option in conversationData.options)
            {
                switch (option)
                {
                    case null:
                    {
                        break;
                    }
                    case GigSpeechConversationOptionDialogue asDialogue:
                    {
                        if (asDialogue.id == id)
                        {
                            //ftm just reinstantiate every time the dialogue Handler
                            //TODO: add a way for the dialogue handler to manage multiple dialogues + answers
                            GameObject dialogue = Instantiate(GigSpeechManager.Instance.parameters.dialoguePrefab);
                            GigSpeechDialogueHandler dialogueHandler = dialogue.GetComponentInChildren<GigSpeechDialogueHandler>();
                            dialogueHandler.thisGigSpeechDialogue = asDialogue.sequence;
                            return true;
                        }
                        break;
                    }
                }
            }

            return false;
        }
    }
}
