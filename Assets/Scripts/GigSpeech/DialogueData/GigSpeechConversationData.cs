using System.Collections.Generic;
//using Unity.VisualScripting;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using UnityEngine.Events;

namespace GigSpeech
{
    [System.Serializable]
    public abstract class GigSpeechConversationOptionBase
    {
        [SerializeField]
        public int id;
        
        [SerializeField]
        public string unlockID;

        public virtual bool IsUnlocked()
        {
            return GigSpeechProgressionManager.Instance.HasUnlockedTag(unlockID);
        }
    }

    [System.Serializable]
    public class GigSpeechConversationOptionDialogue : GigSpeechConversationOptionBase
    {
        [SerializeField]
        public GigSpeechDialogueSequence sequence;
        [SerializeField]
        public List<string> unlockAfterDialogue;
        [SerializeField]
        public UnityEvent callWhenTriggered;
    }

    
    
    [CreateAssetMenu(fileName = "New Conversation", menuName = "GigSpeech/Conversation", order = 51)]
    [System.Serializable]
    public class GigSpeechConversationData : ScriptableObject
    {
        [SerializeField]
        public List<GigSpeechConversationOptionBase> options;
    }
    
#if UNITY_EDITOR
    [CustomEditor(typeof(GigSpeechConversationData))]
    class GigSpeechConversationDataEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GigSpeechConversationData conversationData = (GigSpeechConversationData)target;
            if (conversationData == null)
                return;
            conversationData.options ??= new List<GigSpeechConversationOptionBase>();
            Undo.RecordObject(conversationData, "Change GigSpeechConversationData");
            serializedObject.Update();

            for (int i = 0; i < conversationData.options.Count; i++)
            {
                PrintOption(conversationData.options[i]);
                if (GUILayout.Button("-", GUILayout.Width(30f)))
                {
                    conversationData.options.RemoveAt(i);
                }
            }

            GUILayout.Space(70f);
            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Add Dialogue"))
            {
                conversationData.options.Add(new GigSpeechConversationOptionDialogue());
            }
            GUILayout.EndHorizontal();

            if (GUI.changed)
            {
                EditorUtility.SetDirty(conversationData);
                serializedObject.ApplyModifiedProperties();
            }
        }

        private void PrintOption(GigSpeechConversationOptionBase option)
        {
            //TODO: passer par des SerializedProperty comme dans DialogueSequence
            switch (option)
            {
                case null:
                    return;
                case GigSpeechConversationOptionDialogue asDialogue:
                {
                    GUILayout.Space(50f);
                    GUILayout.BeginVertical();
                    asDialogue.id = EditorGUILayout.IntField("ID", asDialogue.id);
                    asDialogue.unlockID = GUILayout.TextArea(asDialogue.unlockID);
                    asDialogue.sequence = (GigSpeechDialogueSequence)EditorGUILayout.ObjectField("Sequence",
                        asDialogue.sequence, typeof(GigSpeechDialogueSequence), allowSceneObjects: false);

                    asDialogue.unlockAfterDialogue ??= new List<string>();
                    
                    GUILayout.BeginHorizontal();
                    GUILayout.Label("Options to unlock after completion");
                    if (GUILayout.Button("+", GUILayout.Width(30f)))
                    {
                        asDialogue.unlockAfterDialogue.Add("");
                    }
                    GUILayout.EndHorizontal();

                    for(int i = 0; i < asDialogue.unlockAfterDialogue.Count; ++i)
                    {
                        GUILayout.BeginHorizontal();
                        asDialogue.unlockAfterDialogue[i] = GUILayout.TextArea(asDialogue.unlockAfterDialogue[i]);
                        if (GUILayout.Button("-", GUILayout.Width(30f)))
                        {
                            asDialogue.unlockAfterDialogue.RemoveAt(i);
                        }
                        GUILayout.EndHorizontal();
                    }
                    GUILayout.EndVertical();
                    break;
                }
            }
        }
    }
#endif
}
