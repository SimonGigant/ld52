﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GigSpeech
{
    public enum TextSize { Small, Normal, Big }
    public enum TextEffect { None, Wave, Spooky }

    public enum TextSpeed { Pause, Slow, Normal, Fast, Faster, _COUNT}

    [System.Serializable]
    public class GigSpeechDialogueBitData
    {
        public GigSpeechDialogueBitData(GigSpeechDialogueBitData d)
        {
            speaker = d.speaker;
            emotionID = d.emotionID;
            text = d.text;
            overloadedName = d.overloadedName;
            overloadName = d.overloadName;
            leftSide = d.leftSide;
            mirror = d.mirror;
            ignoreFormatting = d.ignoreFormatting;
            overrideListener = d.overrideListener;
            listenerOverridden = d.listenerOverridden;
            listenerEmotionID = d.listenerEmotionID;
            listenerMirror = d.listenerMirror;
        }

        public GigSpeechDialogueBitData()
        {
            speaker = null;
            emotionID = 0;
            text = "";
            leftSide = false;
            mirror = false;
            overrideListener = false;
            listenerOverridden = null;
            listenerEmotionID = 0;
            listenerMirror = false;
        }

        public Sprite portrait => speaker.portraits[emotionID].sprite;
        public Sprite listenerPortrait => overrideListener && listenerOverridden!=null ? listenerOverridden.portraits[listenerEmotionID].sprite : null;
        public string name => overloadName ? overloadedName : speaker.characterName;

        // /!\ When adding any variable, don't forget to change the constructor
        public bool overloadName;
        public string overloadedName;
        //Speaker:
        public GigSpeechSpeakerData speaker;
        public int emotionID;
        public bool mirror;
        public bool leftSide;
        //Listener:
        public bool overrideListener;
        public GigSpeechSpeakerData listenerOverridden;
        public int listenerEmotionID;
        public bool listenerMirror;
        
        //Text:
        public List<GigSpeechEvent> events;
        [TextArea(2, 4)] public string text;
        [HideInInspector] public List<bool> emphasis;
        [HideInInspector] public List<TextSize> size;
        [HideInInspector] public List<TextEffect> effect;
        [HideInInspector] public List<TextSpeed> speed;
        [HideInInspector] public List<bool> ignoreFormatting;
        [HideInInspector] public List<int> triggerEvent;
    }
}