﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GigSpeech
{
    public enum GigSpeechEventType { Custom }

    [Serializable]
    public class GigSpeechEvent
    {
        public GigSpeechEvent()
        {
            type = GigSpeechEventType.Custom;
        }

        public void Trigger()
        {
            switch (type)
            {
                case GigSpeechEventType.Custom:
                {
                    customEvent.Invoke();
                    break;
                }
            }
        }
    
        public UnityEvent customEvent;
        public GigSpeechEventType type;
    }
}