﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace GigSpeech
{
    [CreateAssetMenu(fileName = "New Dialogue Sequence", menuName = "GigSpeech/DialogueSequence", order = 51)]
    [System.Serializable]
    public class GigSpeechDialogueSequence : ScriptableObject
    {
        [HideInInspector] public List<GigSpeechDialogueBitData> dialogues;

        public void Remove(GigSpeechDialogueBitData d)
        {
            dialogues.Remove(d);
        }

        public void Add()
        {
            dialogues.Add(new GigSpeechDialogueBitData());
        }

        public void Switch(GigSpeechDialogueBitData d, bool up)
        {
            int i = dialogues.IndexOf(d);
            i += up ? -1 : 1;
            if (i < 0 || i >= dialogues.Count)
                return;
            dialogues.Remove(d);
            dialogues.Insert(i, d);
        }
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(GigSpeechDialogueSequence))]
    class GigSpeechDialogueSequenceEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            GigSpeechDialogueSequence gigSpeechDialogueSequence = (GigSpeechDialogueSequence)target;
            if (gigSpeechDialogueSequence == null) return;
            gigSpeechDialogueSequence.dialogues ??= new List<GigSpeechDialogueBitData>(); //Not sure if it's really useful, lists should be initialized because the field is serialized
            Undo.RecordObject(gigSpeechDialogueSequence, "Change GigSpeechDialogueSequence");

            serializedObject.Update();
        
            for(int dialogueID = 0; dialogueID < gigSpeechDialogueSequence.dialogues.Count; ++dialogueID)
            {
                GigSpeechDialogueBitData gigSpeechDialogueBitData = gigSpeechDialogueSequence.dialogues[dialogueID];
                SerializedProperty currentDialogueProperty = serializedObject.FindProperty("dialogues").GetArrayElementAtIndex(dialogueID);
            
                GUILayout.Space(50f);
                GUILayout.BeginVertical(); //1 Global dialogue bit block
                GUILayout.BeginHorizontal(); //2 Speaker block
                if(gigSpeechDialogueBitData.leftSide)
                    DrawSpeakerPortrait(gigSpeechDialogueBitData);
                GUILayout.BeginVertical(); //3
                GUILayout.FlexibleSpace();
                GUILayout.BeginHorizontal(); //4
                gigSpeechDialogueBitData.overloadName = GUILayout.Toggle(gigSpeechDialogueBitData.overloadName, new GUIContent("Overload name","When true, will use this name instead of the one specified in the speaker data."));
                if (gigSpeechDialogueBitData.overloadName)
                {
                    gigSpeechDialogueBitData.overloadedName = GUILayout.TextField(gigSpeechDialogueBitData.overloadedName);
                }

                gigSpeechDialogueBitData.mirror = GUILayout.Toggle(gigSpeechDialogueBitData.mirror,  new GUIContent("Mirror", "When true, make the character look the other way"));
                gigSpeechDialogueBitData.leftSide = GUILayout.Toggle(gigSpeechDialogueBitData.leftSide, new GUIContent("Left side","When true, put the character on the left side"));
                GUILayout.EndHorizontal(); //4
                //Speaker:
                GUILayout.BeginHorizontal(); //4
                gigSpeechDialogueBitData.speaker = (GigSpeechSpeakerData)EditorGUILayout.ObjectField("", gigSpeechDialogueBitData.speaker, typeof(GigSpeechSpeakerData), allowSceneObjects: false);
                if (gigSpeechDialogueBitData.speaker != null && gigSpeechDialogueBitData.speaker.portraits.Count > 0)
                {
                    List<string> names = gigSpeechDialogueBitData.speaker.EnumEmotions();
                    int[] ids = new int[names.Count];
                    for (int i = 0; i < names.Count; ++i)
                        ids[i] = i;
                    if (gigSpeechDialogueBitData.emotionID >= names.Count)
                        gigSpeechDialogueBitData.emotionID = 0;
                    if (names.Count != 0)
                        gigSpeechDialogueBitData.emotionID = EditorGUILayout.IntPopup(gigSpeechDialogueBitData.emotionID, names.ToArray(), ids);
                    GUILayout.EndHorizontal(); //4
                    GUILayout.EndVertical(); //3
                    if(!gigSpeechDialogueBitData.leftSide)
                        DrawSpeakerPortrait(gigSpeechDialogueBitData);
                    GUILayout.EndHorizontal(); //2
                }
                else
                {
                    GUILayout.EndHorizontal(); //4
                    GUILayout.EndVertical(); //3
                    GUILayout.EndHorizontal(); //2
                }

                //Listener:
                gigSpeechDialogueBitData.overrideListener =
                    GUILayout.Toggle(gigSpeechDialogueBitData.overrideListener, new GUIContent("Override Listener Portraits","When true, use the given portrait as a listener. Else, keep the previous or next speaker."));
                GUILayout.BeginHorizontal(); //2
                if (gigSpeechDialogueBitData.overrideListener)
                {
                    if (gigSpeechDialogueBitData.listenerOverridden != null && gigSpeechDialogueBitData.listenerOverridden.portraits.Count > 0)
                    {
                        if (!gigSpeechDialogueBitData.leftSide)
                            DrawListenerPortrait(gigSpeechDialogueBitData);

                        GUILayout.BeginVertical(); //3
                        GUILayout.BeginHorizontal(); //4
                        gigSpeechDialogueBitData.listenerMirror = GUILayout.Toggle(
                            gigSpeechDialogueBitData.listenerMirror,
                            new GUIContent("Mirror Listener", "If true, mirror the listener's portrait"));
                        GUILayout.EndHorizontal(); //4
                        GUILayout.BeginHorizontal(); //4
                    }
                    else
                    {
                        GUILayout.BeginVertical(); //3
                        GUILayout.BeginHorizontal(); //4
                    }

                    gigSpeechDialogueBitData.listenerOverridden = (GigSpeechSpeakerData)EditorGUILayout.ObjectField(
                        "Listener", gigSpeechDialogueBitData.listenerOverridden, typeof(GigSpeechSpeakerData), allowSceneObjects: false);
                    
                    if (gigSpeechDialogueBitData.listenerOverridden != null && gigSpeechDialogueBitData.listenerOverridden.portraits.Count > 0)
                    {
                        List<string> names = gigSpeechDialogueBitData.listenerOverridden.EnumEmotions();
                        int[] ids = new int[names.Count];
                        for (int i = 0; i < names.Count; ++i)
                            ids[i] = i;
                        if (gigSpeechDialogueBitData.listenerEmotionID >= names.Count)
                            gigSpeechDialogueBitData.listenerEmotionID = 0;
                        if (names.Count != 0)
                            gigSpeechDialogueBitData.listenerEmotionID =
                                EditorGUILayout.IntPopup(gigSpeechDialogueBitData.listenerEmotionID, names.ToArray(), ids);
                        GUILayout.EndHorizontal(); //4
                        GUILayout.EndVertical(); //3
                        if (gigSpeechDialogueBitData.leftSide)
                            DrawListenerPortrait(gigSpeechDialogueBitData);
                    }
                    else
                    {
                        GUILayout.EndHorizontal(); //4
                        GUILayout.EndVertical(); //3
                    }
                }
                GUILayout.EndHorizontal(); //2

                //Text:
                gigSpeechDialogueBitData.text = GUILayout.TextArea(gigSpeechDialogueBitData.text, GUILayout.Height(3f * EditorGUIUtility.singleLineHeight));
                GUILayout.BeginHorizontal(); //2
                GUILayout.BeginVertical(); //3
                //Events:
                if (gigSpeechDialogueBitData.events != null)
                {
                    for (int i = 0; i < gigSpeechDialogueBitData.events.Count; ++i)
                    {
                        //CustomizeEventData(currentDialogueProperty, i, gigSpeechDialogueBitData);
                    }
                }
                else
                {
                    gigSpeechDialogueBitData.events = new List<GigSpeechEvent>();
                }/*
                if(GUILayout.Button("+", GUILayout.Width(30f)))
                {
                    gigSpeechDialogueBitData.events.Add(new GigSpeechEvent());
                }*/
            
                GUILayout.EndVertical(); //3
                GUILayout.FlexibleSpace();
                if (gigSpeechDialogueSequence.dialogues.IndexOf(gigSpeechDialogueBitData) > 0 && GUILayout.Button(new GUIContent("↑","Move this dialogue line up"), GUILayout.Width(30f)))
                {
                    gigSpeechDialogueSequence.Switch(gigSpeechDialogueBitData, true);
                }
                if (gigSpeechDialogueSequence.dialogues.IndexOf(gigSpeechDialogueBitData) < gigSpeechDialogueSequence.dialogues.Count -1 && GUILayout.Button(new GUIContent("↓", "Move this dialogue line down"), GUILayout.Width(30f)))
                {
                    gigSpeechDialogueSequence.Switch(gigSpeechDialogueBitData, false);
                }
                if (GUILayout.Button(new GUIContent("- DialogueBit","Remove this dialogue line."), GUILayout.Width(120f)))
                {
                    gigSpeechDialogueSequence.Remove(gigSpeechDialogueBitData);
                }
                GUILayout.EndHorizontal(); //2
                GUILayout.EndVertical(); //1
            }
            GUILayout.Space(40f);
            GUILayout.BeginHorizontal(); //1
            GUILayout.FlexibleSpace();
            if (GUILayout.Button(new GUIContent("+ DialogueBit","Add a new dialogue bit to the dialogue."), GUILayout.Width(120f)))
            {
                gigSpeechDialogueSequence.Add();
            }
            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal(); //1

            if (GUI.changed)
            {
                EditorUtility.SetDirty(gigSpeechDialogueSequence);
            }
        }

        private static void DrawListenerPortrait(GigSpeechDialogueBitData gigSpeechDialogueBitData)
        {
            if (gigSpeechDialogueBitData.listenerPortrait != null &&
                gigSpeechDialogueBitData.listenerPortrait.texture != null)
            {
                Rect pos = GUILayoutUtility.GetRect(100, 100);
                if (gigSpeechDialogueBitData.listenerMirror)
                {
                    pos.x += pos.width;
                    pos.width = -pos.width;
                }

                GUI.DrawTexture(pos, gigSpeechDialogueBitData.listenerPortrait.texture);
            }
        }

        private static void DrawSpeakerPortrait(GigSpeechDialogueBitData gigSpeechDialogueBitData)
        {
            if (gigSpeechDialogueBitData.portrait != null && gigSpeechDialogueBitData.portrait.texture != null)
            {
                Rect pos = GUILayoutUtility.GetRect(100, 100);
                if (gigSpeechDialogueBitData.mirror)
                {
                    pos.x += pos.width;
                    pos.width = -pos.width;
                }

                GUI.DrawTexture(pos, gigSpeechDialogueBitData.portrait.texture);
            }
        }

        private void CustomizeEventData(SerializedProperty currentDialogueProperty, int i, GigSpeechDialogueBitData gigSpeechDialogueBitData)
        {
            serializedObject.Update();
            SerializedProperty events = currentDialogueProperty.FindPropertyRelative("events");
            if (events == null)
                return;
        
            GUILayout.BeginHorizontal();
            SerializedProperty dialogueEvent = events.GetArrayElementAtIndex(i);
            EditorGUILayout.PropertyField(dialogueEvent);

            if (GUILayout.Button("-", GUILayout.Width(30f)))
            {
                gigSpeechDialogueBitData.events.RemoveAt(i);
            }

            serializedObject.ApplyModifiedProperties();
            GUILayout.EndHorizontal();
        }
    }
#endif
}