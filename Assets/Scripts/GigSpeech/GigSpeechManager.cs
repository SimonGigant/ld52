using UnityEngine;
using UnityEngine.Events;

namespace GigSpeech
{
    public class GigSpeechManager : MonoBehaviour
    {
        private static GigSpeechManager _instance;

        public static GigSpeechManager Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<GigSpeechManager>();
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("GigSpeechManager");
                        DontDestroyOnLoad(container);
                        _instance = container.AddComponent<GigSpeechManager>();
                    }
                }
                return _instance;
            }
        }

        public UnityEvent OnTryPass;

        public void Pass()
        {
            OnTryPass.Invoke();;
        }
        
        public GigSpeechParameters parameters;
    }
}
