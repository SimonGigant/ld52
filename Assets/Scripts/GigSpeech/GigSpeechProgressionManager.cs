using System;
using UnityEngine;
using System.Collections.Generic;

namespace GigSpeech
{
    public class GigSpeechProgressionManager : MonoBehaviour
    {
        private static GigSpeechProgressionManager _instance;

        public static GigSpeechProgressionManager Instance
        {
            get {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<GigSpeechProgressionManager>();
                    if (_instance == null)
                    {
                        GameObject container = new GameObject("ProgressionManager");
                        DontDestroyOnLoad(container);
                        _instance = container.AddComponent<GigSpeechProgressionManager>();
                    }
                }
                return _instance;
            }
        }
        
        private HashSet<string> _unlockedTags;

        private void Awake()
        {
            _unlockedTags = new HashSet<string>();
        }


        public void UnlockTag(string unlockTag)
        {
            if(!_unlockedTags.Contains(unlockTag))
                _unlockedTags.Add(unlockTag);
        }

        public bool HasUnlockedTag(string unlockedTag)
        {
            return _unlockedTags.Contains(unlockedTag);
        }

        public bool HasUnlockedAny(string[] unlockedTags)
        {
            foreach(string s in unlockedTags)
            {
                if (_unlockedTags.Contains(s))
                    return true;
            }
            return false;
        }

        public bool HasUnlockedAll(string[] unlockedTags)
        {
            foreach (string s in unlockedTags)
            {
                if (!_unlockedTags.Contains(s))
                    return false;
            }

            return true;
        }
    }
}
