﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GigSpeech
{
    public enum GigSpeechPortraitAnimType {None, PuppetRotation, PuppetTranslation, Scale}
    public enum GigSpeechPortraitTransitionType { None, Basic}

    [CreateAssetMenu(fileName = "New Parameter", menuName = "GigSpeech/GigSpeechParameters")]
    [Serializable]
    public class GigSpeechParameters : ScriptableObject
    {
        [SerializeField]
        public GameObject dialoguePrefab;

        [Header("Box")]
        [SerializeField] public Sprite baseBoxSprite;
        [SerializeField] public bool changeBoxSpriteFromDirection;
        [SerializeField] public Sprite leftBoxSprite;
        [SerializeField] public Sprite rightBoxSprite;
        [SerializeField] public bool twinklingDotWhenCanPass;
        
        [Header("Portraits")]
        [SerializeField] public bool showListener = true;
        [SerializeField] public Color listenerMultiplyColor = Color.grey;
        [SerializeField] public GigSpeechPortraitAnimType animType;
        [SerializeField] public GigSpeechPortraitTransitionType transitionType;
        public float portraitShakeIntensity = 1.0f;
        public float durationAnimationEnteringBox = 0.6f;
        [Header("Text")]
        public Color baseColor;
        public Color colorAparte;
        public float delayBetweenLetters = 1.0f;
        public bool autoPass = true;
        public float autoPassDuration = 3.0f;
        public float letterAnimationIntensity = 1.0f;
    }
}