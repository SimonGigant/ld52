﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
//using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;
using UnityEngine.UI;
using Sequence = DG.Tweening.Sequence;

namespace GigSpeech
{
    [RequireComponent(typeof(GigSpeechUIShake))]
    public class GigSpeechDialogueHandler : MonoBehaviour
    {
        public GigSpeechDialogueSequence thisGigSpeechDialogue;


        private List<GigSpeechDialogueBitData> _dialogueData;
        private bool _textIsPrinted = false;
        private float _counter = 0.0f;
        private DOTweenTMPAnimator _animator;
        private Sequence _sequence;
        private Sequence _sequencePortraitSpeaker;
        private Sequence _sequencePortraitListener;
        private string _speakerNameL;
        private string _speakerNameR;
        private bool _isTwinkling = false;
        private Coroutine _twinkle;
        private float _scalePortrait;

        private bool _speakerIsRight = true;
        //Cache :
        private bool _willReappearPortrait = true;


        [Header("Fields")]
        [SerializeField] private TMP_Text text = null;
        [SerializeField] private TMP_Text speakerName = null;
        [SerializeField] private Image portraitR = null;
        [SerializeField] private Image portraitL = null;
        [SerializeField] private Image endDialogueSign = null;
        [SerializeField] private Material shaderNormal = null;
        [SerializeField] private Image Box = null;


        [SerializeField] public UnityEvent callbackAfterDialogue = null;


        private void Start()
        {
            DOTween.SetTweensCapacity(1250,250);
            GetComponentInParent<Canvas>().worldCamera = Camera.main;
            _scalePortrait = 1.0f;

            if (GigSpeechManager.Instance.parameters.baseBoxSprite != null)
            {
                Box.sprite = GigSpeechManager.Instance.parameters.baseBoxSprite;
            }

            if (GigSpeechManager.Instance.parameters.showListener)
            {
                if (_speakerIsRight)
                    portraitL.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                else
                    portraitR.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
            }
            else
            {
                //Hide the listener sprite
                Color c = _speakerIsRight ? portraitL.color : portraitR.color;
                c.a = 0f;
                if (_speakerIsRight)
                    portraitL.color = c;
                else
                    portraitR.color = c;
            }
            
            if (GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.Basic)
            {
                Color cR = portraitR.color;
                cR.a = 0f;
                portraitR.color = cR;
                Color cL = portraitL.color;
                cL.a = 0f;
                portraitL.color = cL;
            }
            GigSpeechManager.Instance.OnTryPass.AddListener(PassDialogue);
            StartCoroutine(TimerBeforeBegin(GigSpeechManager.Instance.parameters.durationAnimationEnteringBox));
        }

        /**
         * Wait the duration before starting the dialogue
         */
        private IEnumerator TimerBeforeBegin(float duration)
        {
            transform.DOLocalMoveY(-2000.0f, GigSpeechManager.Instance.parameters.durationAnimationEnteringBox).From().SetEase(Ease.OutQuad);
            StartDialogue(thisGigSpeechDialogue);
            yield return new WaitForSeconds(duration);
            UpdateDisplay();
        }

        /**
         * Wait the duration before removing the bubble
         */
        private IEnumerator TimerBeforeEnd(float duration)
        {
            /*make the player not move*/
            transform.DOLocalMoveY(-2000f, GigSpeechManager.Instance.parameters.durationAnimationEnteringBox).SetEase(Ease.InQuad);
            yield return new WaitForSeconds(duration);
            Destroy(transform.parent.gameObject);
        }

        private void Update()
        {
            _counter += Time.deltaTime;
            if (_textIsPrinted && !_isTwinkling && GigSpeechManager.Instance.parameters.twinklingDotWhenCanPass)
            {
                _twinkle = StartCoroutine(Twinkle());
            }
            if (_textIsPrinted && GigSpeechManager.Instance.parameters.autoPass && _counter > GigSpeechManager.Instance.parameters.autoPassDuration)
            {
                PassDialogue();
            }
        }

    /**
     * Translate the tag-based text into the printed text
     * and cache where the different effects will be called
     */
        private GigSpeechDialogueBitData TranslateFx(GigSpeechDialogueBitData bitData)
        {
            GigSpeechDialogueBitData res = new GigSpeechDialogueBitData(bitData)
            {
                emphasis = new List<bool>(),
                size = new List<TextSize>(),
                effect = new List<TextEffect>(),
                speed = new List<TextSpeed>()
            };
            string thisText = "";
            bool isEmphasized = false;
            bool ignoreFormatting = false;
            TextSize currentSize = TextSize.Normal;
            TextEffect currentEffect = TextEffect.None;
            TextSpeed currentSpeed = TextSpeed.Normal;
            TextSpeed previousSpeed = TextSpeed.Normal;
            bool isPause = false;
            foreach(char c in res.text)
            {
                if (!isPause && currentSpeed == TextSpeed.Pause)
                {
                    currentSpeed = previousSpeed;
                }
                if (!ignoreFormatting || c == '\\')
                {
                    switch (c)
                    {
                        case '*':
                        {
                            isEmphasized = !isEmphasized;
                            if (isEmphasized)
                            {
                                thisText += "<b>";
                            }
                            else
                            {
                                thisText += "</b>";
                            }
                            continue;
                        }
                        case '_':
                        {
                            currentSize = currentSize == TextSize.Small ? TextSize.Normal: TextSize.Small;
                            continue;
                        }
                        case '^':
                        {
                            currentSize = currentSize == TextSize.Big ? TextSize.Normal : TextSize.Big;
                            continue;
                        }
                        case '~':
                        {
                            currentEffect = currentEffect == TextEffect.Wave ? TextEffect.None : TextEffect.Wave;
                            continue;
                        }
                        case '°':
                        {
                            currentEffect = currentEffect == TextEffect.Spooky ? TextEffect.None : TextEffect.Spooky;
                            continue;
                        }
                        case '>':
                        {
                            if (currentSpeed < TextSpeed._COUNT - 1)
                            {
                                ++currentSpeed;
                            }
                            continue;
                        }
                        case '<':
                        {
                            if (currentSpeed > TextSpeed.Pause + 1)
                            {
                                --currentSpeed;
                            }
                            continue;
                        }
                        case '|':
                        {
                            previousSpeed = currentSpeed == TextSpeed.Pause ? TextSpeed.Normal : currentSpeed;
                            currentSpeed = TextSpeed.Pause;
                            isPause = true;
                            continue;
                        }
                        case '\\':
                        {
                            ignoreFormatting = !ignoreFormatting;
                            continue;
                        }
                    }
                }
                //Only if this is not a balise
                thisText += c;
                res.emphasis.Add(isEmphasized);
                res.size.Add(currentSize);
                res.effect.Add(currentEffect);
                res.speed.Add(currentSpeed);
                res.ignoreFormatting.Add(ignoreFormatting);
                isPause = false;
            }
            res.text = thisText;
            return res;
        } 
        
    /**
     * Initialize the bubble and portrait before the first dialogue
     */
        private void StartDialogue(GigSpeechDialogueSequence d)
        {
            if (d == null)
                return;
            _dialogueData = new List<GigSpeechDialogueBitData>();
            foreach(GigSpeechDialogueBitData data in d.dialogues)
            {
                _dialogueData.Add(TranslateFx(data));
            }

            _speakerIsRight = !_dialogueData[0].leftSide;
            
            speakerName.text = _dialogueData[0].name;
            Material matName = speakerName.fontMaterial;
            matName.SetColor(ShaderUtilities.ID_UnderlayColor, _dialogueData[0].speaker.color);
            speakerName.fontMaterial = matName;
            

            if (_speakerIsRight)
            {
                _speakerNameR = _dialogueData[0].name;
                portraitR.sprite = _dialogueData[0].portrait;
                portraitR.rectTransform.pivot = new Vector2(
                    _dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width,
                    0);
                portraitR.material = shaderNormal;
                portraitR.transform.localScale = _dialogueData[0].mirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);

                //setting the listener sprite to empty, it could be overriden after if there is actually a listener
                portraitL.sprite = null;
            }
            else
            {
                _speakerNameL = _dialogueData[0].name;
                portraitL.sprite = _dialogueData[0].portrait;
                portraitL.rectTransform.pivot = new Vector2(
                    _dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width, 
                    0);
                portraitL.material = shaderNormal;
                portraitL.transform.localScale = _dialogueData[0].mirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                
                //setting the listener sprite to empty, it could be overriden after if there is actually a listener
                portraitR.sprite = null;
            }
            
            //Listener
            if (_dialogueData[0].overrideListener && _dialogueData[0].listenerOverridden!=null)
            {
                if (_speakerIsRight)
                {
                    _speakerNameL = _dialogueData[0].name;
                    portraitL.sprite = _dialogueData[0].listenerPortrait;
                    portraitL.rectTransform.pivot = new Vector2(
                        _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
                        0);
                    portraitL.transform.localScale = _dialogueData[0].listenerMirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                }
                else
                {
                    _speakerNameR = _dialogueData[0].name;
                    portraitR.sprite = _dialogueData[0].listenerPortrait;
                    portraitR.rectTransform.pivot = new Vector2(
                        _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
                        0);
                    portraitR.transform.localScale = _dialogueData[0].listenerMirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);

                }
            }
            else
            {
                if (_speakerIsRight)
                {
                    for(int i = 1; i < _dialogueData.Count; ++i)
                    {
                        if (_dialogueData[i].speaker != _dialogueData[0].speaker && _dialogueData[i].leftSide != _dialogueData[0].leftSide)
                        {
                            _speakerNameL = _dialogueData[i].name;
                            portraitL.sprite = _dialogueData[i].portrait;
                            portraitL.rectTransform.pivot = new Vector2(
                                _dialogueData[i].portrait.pivot.x / _dialogueData[i].portrait.rect.width,
                                0);
                            portraitL.transform.localScale = _dialogueData[i].mirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                            break;
                        }
                    }
                }
                else
                {
                    for(int i = 1; i < _dialogueData.Count; ++i)
                    {
                        if (_dialogueData[i].speaker != _dialogueData[0].speaker && _dialogueData[i].leftSide != _dialogueData[0].leftSide)
                        {
                            _speakerNameR = _dialogueData[i].name;
                            portraitR.sprite = _dialogueData[i].portrait;
                            portraitR.rectTransform.pivot = new Vector2(
                                _dialogueData[i].portrait.pivot.x / _dialogueData[i].portrait.rect.width,
                                0);
                            portraitR.transform.localScale = _dialogueData[i].mirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                            break;
                        }
                    }
                }
            }

            if (portraitL.sprite == null)
            {
                Color c = portraitL.color;
                c.a = 0f;
                portraitL.color = c;
            }
            if (portraitR.sprite == null)
            {
                Color c = portraitR.color;
                c.a = 0f;
                portraitR.color = c;
            }
            text.SetText("");
        }


        /**
     * Pass to the next bubble of dialogue
     */
        private void PassDialogue()
        {
            if(_textIsPrinted && _dialogueData.Count > 0)
            {
                if(_twinkle != null)
                    StopCoroutine(_twinkle);
                _isTwinkling = false;
                endDialogueSign.enabled = false;
                //For transitions
                if(_dialogueData.Count > 1)
                {
                    /*if (!GigSpeechManager.Instance.parameters.showListener)
                    {
                        _willReappearPortrait = _dialogueData[0].speaker != _dialogueData[1].speaker;
                    }
                    else
                    {
                        if (_dialogueData[1].leftSide)
                        {
                            _willReappearPortrait = _dialogueData[1].speaker.characterName != _speakerNameL;
                        }
                        else
                        {
                            _willReappearPortrait = _dialogueData[1].speaker.characterName != _speakerNameR;
                        }
                    }*/
                    _willReappearPortrait = false;
                }
                _dialogueData.RemoveAt(0);
                UpdateDisplay();
                //SoundManager.Instance.PlayDialogNext();
            }
            /*else if (!_textIsPrinted)
            {
                _sequence.Complete();
                _sequencePortraitSpeaker.Complete();
                _sequencePortraitListener.Complete();
                //SoundManager.Instance.PlayDialogSkip();
            }*/
            else if(_textIsPrinted && _dialogueData.Count == 0)
            {
                _textIsPrinted = false;
                callbackAfterDialogue.Invoke();
            }
        }

        /**
     * Subscribe given event that needed to be called after the dialogue
     */
        public void SubscribeToCallbackAfterDialogue(UnityAction call)
        {
            callbackAfterDialogue.AddListener(call);
        }

        /**
     * Unsubscribe given event that needed to be called after the dialogue
     */
        public void UnsubscribeFromCallbackAfterDialogue(UnityAction call)
        {
            callbackAfterDialogue.RemoveListener(call);
        }

        /**
     * Call when the player want to pass the dialogue
     */
        private void PassAction(InputAction.CallbackContext ctx)
        {
            //if(!PauseMenu.isPaused)
            PassDialogue();
        }

        //TODO: develop different types of twinkle
        /**
     * Start the twinkling to notify the player that they can pass the dialogue
     */
        private IEnumerator Twinkle()
        {
            _isTwinkling = true;
            for(; ; )
            {
                yield return new WaitForSeconds(0.5f);
                endDialogueSign.enabled = !endDialogueSign.enabled;
            }
        }

        /**
         * Update the bubble to show the next dialogue
         * Trigger the end of the bubble if there is none
         */
        private void UpdateDisplay()
        {
            if (_dialogueData.Count <= 0)
            {
                callbackAfterDialogue.Invoke();
                StartCoroutine(TimerBeforeEnd(GigSpeechManager.Instance.parameters.durationAnimationEnteringBox));
                return;
            }
            text.SetText(_dialogueData[0].text);
            speakerName.text = _dialogueData[0].name;
            _speakerIsRight = !_dialogueData[0].leftSide;
            if (_speakerIsRight)
                _speakerNameR = _dialogueData[0].name;
            else
                _speakerNameL = _dialogueData[0].name;
            
            
            if (GigSpeechManager.Instance.parameters.changeBoxSpriteFromDirection && GigSpeechManager.Instance.parameters.leftBoxSprite != null && GigSpeechManager.Instance.parameters.rightBoxSprite != null)
            {
                Box.sprite = _speakerIsRight
                    ? GigSpeechManager.Instance.parameters.rightBoxSprite
                    : GigSpeechManager.Instance.parameters.leftBoxSprite;
            }
            
            Material matName = speakerName.fontMaterial;
            matName.SetColor(ShaderUtilities.ID_UnderlayColor, _dialogueData[0].speaker.color);
            speakerName.fontMaterial = matName;
            
            if (_speakerIsRight)
            {
                portraitR.sprite = _dialogueData[0].portrait;
                portraitR.rectTransform.pivot = new Vector2(_dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width, 0);
                portraitR.material = shaderNormal;
                portraitR.transform.localScale = _dialogueData[0].mirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                portraitR.color = Color.white;
                
                if (GigSpeechManager.Instance.parameters.showListener)
                {
                    portraitL.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                    portraitL.material = shaderNormal;

                    if (_dialogueData[0].overrideListener && _dialogueData[0].listenerOverridden!=null)
                    {
                        _speakerNameL = _dialogueData[0].listenerOverridden.characterName;
                        portraitL.sprite = _dialogueData[0].listenerPortrait;
                        portraitL.rectTransform.pivot = new Vector2(
                            _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
                            0);
                        portraitL.transform.localScale = _dialogueData[0].listenerMirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                    }
                }
                else
                {
                    Color c = portraitL.color;
                    c.a = 0.0f;
                    portraitL.color = c;
                }
            }
            else
            {
                portraitL.sprite = _dialogueData[0].portrait;
                portraitL.rectTransform.pivot = new Vector2(_dialogueData[0].portrait.pivot.x / _dialogueData[0].portrait.rect.width, 0);
                portraitL.material = shaderNormal;
                portraitL.transform.localScale = _dialogueData[0].mirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                portraitL.color = Color.white;
                
                if (GigSpeechManager.Instance.parameters.showListener)
                {
                    portraitR.color = GigSpeechManager.Instance.parameters.listenerMultiplyColor;
                    if (_dialogueData[0].overrideListener && _dialogueData[0].listenerOverridden!=null)
                    {
                        _speakerNameR = _dialogueData[0].listenerOverridden.characterName;
                        portraitR.sprite = _dialogueData[0].listenerPortrait;
                        portraitR.rectTransform.pivot = new Vector2(
                            _dialogueData[0].listenerPortrait.pivot.x / _dialogueData[0].listenerPortrait.rect.width,
                            0);
                        portraitR.transform.localScale = _dialogueData[0].listenerMirror ? new Vector2(-_scalePortrait, _scalePortrait) : new Vector2(_scalePortrait, _scalePortrait);
                    }
                }
                else
                {
                    Color c = portraitR.color;
                    c.a = 0.0f;
                    portraitR.color = c;
                }
            }

            if (portraitL.sprite == null)
            {
                Color c = portraitL.color;
                c.a = 0f;
                portraitL.color = c;
            }
            if (portraitR.sprite == null)
            {
                Color c = portraitR.color;
                c.a = 0f;
                portraitR.color = c;
            }
            
            _textIsPrinted = false;
            text.ForceMeshUpdate();
            FxShowTextCharByChar(text);
            text.ForceMeshUpdate();
        }

        /**
     * Shake the UI for the bubble
     */
        private void Shake()
        {
            GetComponent<GigSpeechUIShake>().Shake();
        }

        /**
     * Called on the end of the bubble
     */
        private void FinishPrint()
        {
            _textIsPrinted = true;
            _counter = 0.0f;
        }

        /**
         * The actual tweening on the characters and portraits
         * Plan the sequences for this bubble, and use the cached FX values to apply effects on the text
         */
        private void FxShowTextCharByChar(TMP_Text currentText)
        {
            _sequence = DOTween.Sequence().SetAutoKill(true);
            _sequencePortraitSpeaker = DOTween.Sequence().SetAutoKill(true);
            RectTransform portraitAnchor = _speakerIsRight
                ? portraitR.GetComponent<RectTransform>()
                : portraitL.GetComponent<RectTransform>();
            _animator ??= new DOTweenTMPAnimator(currentText);

            _sequencePortraitListener = DOTween.Sequence().SetAutoKill(true);
            RectTransform portraitAnchorListener = _speakerIsRight
                ? portraitL.GetComponent<RectTransform>()
                : portraitR.GetComponent<RectTransform>();

            int start = 0;
            int end = _animator.textInfo.characterCount;
            float offsetCumul = 0.0f;
            char previousChar = '\0';
        
            Color textColor = GigSpeechManager.Instance.parameters.baseColor;
            textColor.a = 0.0f;
            currentText.color = textColor;

            for (int i = start; i < end; ++i)
            {
                float currentTimeOffset = GigSpeechManager.Instance.parameters.delayBetweenLetters * 0.05f;
                Vector3 currentPosOffset = _animator.GetCharOffset(i);
                float speedMultiplier = 1f;
                Sequence portraitSequence = DOTween.Sequence().SetAutoKill(true);
                Sequence portraitSequenceListener = DOTween.Sequence().SetAutoKill(true);
                Sequence charSequence = DOTween.Sequence().SetAutoKill(true);
                int portraitMoveType = 0;
                char thisChar = _animator.textInfo.characterInfo[i].character;
                UnityEvent onComplete = new UnityEvent();

                //Set speed multiplier depending on the tags
                switch (_dialogueData[0].speed[i])
                {
                    case TextSpeed.Pause:
                    {
                        speedMultiplier *= 10f;
                        break;
                    }
                    case TextSpeed.Slow:
                    {
                        speedMultiplier *= 3f;
                        break;
                    }
                    case TextSpeed.Fast:
                    {
                        speedMultiplier *= 0.5f;
                        break;
                    }
                    case TextSpeed.Faster:
                    {
                        speedMultiplier *= 0.2f;
                        break;
                    }
                }
                currentTimeOffset *= speedMultiplier;


                //Get previous character
                if (i != 0)
                    previousChar = _animator.textInfo.characterInfo[i-1].character;
                else
                    portraitMoveType = -1;

                if (_animator.textInfo.characterInfo[i].isVisible)
                {
                    //Set the tween on the text depending on the tag-set size
                    switch (_dialogueData[0].size[i])
                    {
                        case TextSize.Small:
                        {
                            if(portraitMoveType != -1)
                                portraitMoveType = -2; //Small fix to not animate on aparte
                            charSequence.Join(_animator
                                    .DOOffsetChar(i,
                                        currentPosOffset + new Vector3(Random.Range(-0.4f, 0.4f),
                                            6f * GigSpeechManager.Instance.parameters.letterAnimationIntensity, 0), 0.1f * speedMultiplier).From()
                                    .SetEase(Ease.OutBack)).OnPlay(onComplete.Invoke)
                                .Join(_animator.DOScaleChar(i, 0.65f, 0f))
                                .Join(_animator.DOColorChar(i, GigSpeechManager.Instance.parameters.colorAparte, 0.01f * speedMultiplier))
                                .Join(_animator.DOFadeChar(i, 1.0f, 0.01f * speedMultiplier).SetEase(Ease.OutQuad));
                            currentTimeOffset *= 1.4f;
                            break;
                        }
                        case TextSize.Big:
                        {
                            charSequence.Join(_animator
                                    .DOOffsetChar(i,
                                        currentPosOffset + new Vector3(Random.Range(-10f, 10f), 16f * GigSpeechManager.Instance.parameters.letterAnimationIntensity, 0),
                                        0.1f * speedMultiplier).From()
                                    .SetEase(Ease.OutBack)).OnPlay(onComplete.Invoke)
                                .Join(_animator.DOScaleChar(i, 1.25f, 0f))
                                .Join(_animator.DOColorChar(i, GigSpeechManager.Instance.parameters.baseColor, 0.01f * speedMultiplier))
                                .Join(_animator.DOFadeChar(i, 1.0f, 0.01f * speedMultiplier).SetEase(Ease.OutQuad));
                            currentTimeOffset *= 1.6f;
                            break;
                        }
                        default:
                        {
                            charSequence.Join(_animator.DOOffsetChar(i, currentPosOffset + new Vector3(Random.Range(-10f, 10f), 12f * GigSpeechManager.Instance.parameters.letterAnimationIntensity, 0), 0.1f * speedMultiplier).From()
                                    .SetEase(Ease.OutBack))
                                .OnPlay(onComplete.Invoke)
                                .Join(_animator.DOScaleChar(i, 1f, 0f))
                                .Join(_animator.DOColorChar(i, GigSpeechManager.Instance.parameters.baseColor, 0.01f * speedMultiplier))
                                .Join(_animator.DOFadeChar(i, 1.0f, 0.01f * speedMultiplier).SetEase(Ease.OutQuad));
                            break;
                        }
                    }
                }

                //Check punctuation
                if (i != 0 && !_dialogueData[0].ignoreFormatting[i]) {
                    //For hard stop
                    if (previousChar == '.' && thisChar != '.'
                        || previousChar == ':' || previousChar == ';' || previousChar == '?')
                    {
                        currentTimeOffset *= 13f;
                    }
                    //For pause
                    else if (previousChar == ',')
                    {
                        currentTimeOffset *= 11f;
                    }
                    //For exclamation
                    else if (thisChar == '!' && !_dialogueData[0].emphasis[i])
                    {
                        charSequence
                            .Join(_animator.DOScaleChar(i, new Vector3(2.0f, 2.0f, 0.0f), 0.4f).From().SetEase(Ease.OutBounce))
                            .OnStart(Shake);
                        if (portraitMoveType == 0)
                            portraitMoveType = 1;
                    }
                    //For the end of a small text
                    else if (_dialogueData[0].size[i - 1] == TextSize.Small && _dialogueData[0].size[i] != TextSize.Small)
                    {
                        currentTimeOffset *= 14f;
                    }
                    //For after an exclamation
                    else if (previousChar == '!')
                    {
                        currentTimeOffset *= 13f;
                    }
                }

                //Check next char
                if (i != end && _animator.textInfo.characterInfo.Length > i+1)
                {
                    char nextChar = _animator.textInfo.characterInfo[i+1].character;
                    //To make the exclamation hit sooner and have more impact
                    if (nextChar == '!' && thisChar == ' ')
                        currentTimeOffset *= 0.3f;
                    //End of a word
                    if (nextChar == ' ')
                    {
                        if(portraitMoveType != -1)
                            portraitMoveType = 2;
                    }
                }
                //This is the last letter of the text
                else
                    portraitMoveType = 2;
                
                if(thisChar == ' ' && portraitMoveType != -1)
                    portraitMoveType = -2; //Small fix

                    //If it was tagged on emphasis
                if (_dialogueData[0].emphasis[i])
                {
                    currentTimeOffset *= 2f;
                    //charSequence.Join(_tweener.DOScaleY(i,5f, 0.15f * speedMultiplier).From().SetEase(Ease.InOutQuad)).OnStart(Shake);
                    charSequence
                        .Join(_animator.DOScaleChar(i, new Vector3(3, 3, 0), 0.15f * speedMultiplier).From()
                            .SetEase(Ease.OutBack)).OnStart(Shake);
                    if (portraitMoveType == 0)
                        portraitMoveType = 1;
                }
                //Special effect: ~Wave~
                if (_dialogueData[0].effect[i] == TextEffect.Wave)
                {
                    Tween sine = _animator.DOOffsetChar(i, new Vector3(0.0f,6,0), 0.4f).SetEase(Ease.InOutSine)
                        .SetLoops(3, LoopType.Yoyo).SetAutoKill(true);
                    Tween endSine = _animator.DOOffsetChar(i, Vector3.zero, 0.5f).SetEase(Ease.InOutSine).SetAutoKill(true);
                    charSequence.Append(sine);
                    charSequence.Append(endSine);
                }
                //Special effect: ṡ̵͇̎̕p̴̞̞̰̘̎͒̽͝o̷̹̙̒̀̈́͐o̵̪̪͍̽͂̏͝ͅk̴͍̓y̸̱̣̪͈͍͑̈
                else if (_dialogueData[0].effect[i] == TextEffect.Spooky)
                {
                    Sequence spook = DOTween.Sequence().SetAutoKill(true);
                    spook.Append(_animator.DOShakeCharOffset(i, 1.0f, 3.0f, 105, 180f));
                    charSequence.Append(spook);
                }

                PortraitAnim(ref portraitSequence, portraitMoveType, portraitAnchor, _dialogueData[0].leftSide ? -1f : 1f);
                _sequence.Insert(currentTimeOffset + offsetCumul, charSequence);
                _sequencePortraitSpeaker.Insert(currentTimeOffset + offsetCumul, portraitSequence);
                
                
                if (GigSpeechManager.Instance.parameters.showListener && portraitMoveType == -1)
                {
                    PortraitAnim(ref portraitSequenceListener, -1, portraitAnchorListener, _dialogueData[0].leftSide ? 1f : -1f, true);
                    _sequencePortraitListener.Insert(currentTimeOffset + offsetCumul + 0.1f, portraitSequenceListener);
                }
                
                
                offsetCumul += currentTimeOffset;
            }
            _sequence.OnComplete(FinishPrint);
        }
        
        
    //************************
    // Portrait animations:


        /**
         * Animate the portrait
         * with the given type of move, and the given anchor
         */
        private void PortraitAnim(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor, float direction, bool isListener = false)
        {
            switch (GigSpeechManager.Instance.parameters.transitionType)
            {
                case GigSpeechPortraitTransitionType.Basic:
                {
                    PortraitAnimTransitionBasic(ref portraitSequence, portraitMoveType, portraitAnchor, direction, isListener);
                    break;
                }
            }
        
            switch (GigSpeechManager.Instance.parameters.animType)
            {
                case GigSpeechPortraitAnimType.PuppetRotation:
                {
                    PortraitAnimPuppetRotation(ref portraitSequence, portraitMoveType, portraitAnchor);
                    break;
                }
                case GigSpeechPortraitAnimType.PuppetTranslation:
                {
                    PortraitAnimPuppetTranslation(ref portraitSequence, portraitMoveType, portraitAnchor);
                    break;
                }
                case GigSpeechPortraitAnimType.Scale:
                {
                    PortraitAnimScale(ref portraitSequence, portraitMoveType, portraitAnchor);
                    break;
                }
            }
        }

        /**
         * Portrait animation with rotations around the anchor (like MatPat in Game Theory)
         */
        private void PortraitAnimPuppetRotation(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor)
        {
            //Types are:
            //0: basic
            //1: intense
            //2: end of a word
            switch (portraitMoveType)
            {
                case 1:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.forward * (Random.Range(0f,1f) * 25.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity), 0.03f).SetEase(Ease.OutBounce));
                    break;
                }
                case 2:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.forward * (Random.Range(0f, 1f) * 10.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity), 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.zero, 0.1f).SetEase(Ease.OutBounce));
                    break;
                }
                default:
                {
                    if (portraitMoveType >= 0 || GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.None)
                    {
                        portraitSequence.Append(portraitAnchor.DOLocalRotate(Vector3.forward * (Random.Range(0f, 1f) * 10.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity), 0.03f).SetEase(Ease.OutBounce));
                    }
                    break;
                }
            }
        }

        public void PlaySound()
        {
            if (AudioManager.Instance != null)
            {
                AudioManager.Instance.PlayVoiceSound();
            }
        }

        /**
         * Portrait animation with Y scale (stretch)
         */
        private void PortraitAnimScale(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor)
        {
            switch (portraitMoveType)
            {
                case 1:
                {
                    float scaleTargetY = _scalePortrait * Random.Range(1f - 0.1f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 1f + 0.1f * GigSpeechManager.Instance.parameters.portraitShakeIntensity);
                    portraitSequence.Append(portraitAnchor.DOScaleY(scaleTargetY, 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Join(portraitAnchor.DOScaleX(Mathf.Sign(portraitAnchor.transform.localScale.x) * (2f * _scalePortrait - scaleTargetY), 0.03f).SetEase(Ease.OutBounce));

                    
                    portraitSequence.OnComplete(PlaySound);
                    break;
                }
                case 2:
                {
                    float scaleTargetY = _scalePortrait * Random.Range(1f - 0.07f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 1f + 0.07f * GigSpeechManager.Instance.parameters.portraitShakeIntensity);
                    portraitSequence.Append(portraitAnchor.DOScaleY(scaleTargetY, 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Join(portraitAnchor.DOScaleX(Mathf.Sign(portraitAnchor.transform.localScale.x) * (2f * _scalePortrait - scaleTargetY), 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Append(portraitAnchor.DOScaleY(_scalePortrait, 0.1f).SetEase(Ease.OutBounce));
                    portraitSequence.Join(portraitAnchor.DOScaleX(_scalePortrait * Mathf.Sign(portraitAnchor.transform.localScale.x), 0.1f).SetEase(Ease.OutBounce));
                    
                    portraitSequence.OnComplete(PlaySound);
                    break;
                }
                default:
                {
                    if (portraitMoveType >= 0 || GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.None)
                    {
                        float scaleTargetY = _scalePortrait * Random.Range(1f - 0.05f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 1f + 0.05f * GigSpeechManager.Instance.parameters.portraitShakeIntensity);
                        portraitSequence.Append(portraitAnchor.DOScaleY(scaleTargetY, 0.03f).SetEase(Ease.OutBounce));
                        portraitSequence.Join(portraitAnchor.DOScaleX(Mathf.Sign(portraitAnchor.transform.localScale.x) * (2f * _scalePortrait - scaleTargetY), 0.03f).SetEase(Ease.OutBounce));
                        
                        portraitSequence.OnComplete(PlaySound);
                    }
                    break;
                }
            }
        }

        /**
         * Portrait animation with x translations (gliding)
         */
        private void PortraitAnimPuppetTranslation(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor)
        {
            switch (portraitMoveType)
            {
                case 1:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalMoveX(Random.Range(-1f, 1f) * 40.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 0.03f).SetEase(Ease.OutBounce));
                    break;
                }
                case 2:
                {
                    portraitSequence.Append(portraitAnchor.DOLocalMoveX(Random.Range(-1f, 1f) * 20.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 0.03f).SetEase(Ease.OutBounce));
                    portraitSequence.Append(portraitAnchor.DOLocalMoveX(0f, 0.1f).SetEase(Ease.OutBounce));
                    break;
                }
                default:
                {
                    if (portraitMoveType >= 0 || GigSpeechManager.Instance.parameters.transitionType == GigSpeechPortraitTransitionType.None)
                    {
                        portraitSequence.Append(portraitAnchor.DOLocalMoveX(Random.Range(-1f, 1f) * Random.Range(0f, 1f) * 20.0f * GigSpeechManager.Instance.parameters.portraitShakeIntensity, 0.03f).SetEase(Ease.OutBounce));
                    }
                    break;
                }
            }
        }

        /**
         * Enter with a glide from outside of the screen
         */
        private void PortraitAnimTransitionBasic(ref Sequence portraitSequence, int portraitMoveType, RectTransform portraitAnchor, float direction, bool isListener)
        {
            if(portraitMoveType == -1)
            {
                if (_willReappearPortrait)
                {
                    Vector3 randomRotation = -Vector3.forward * ((Random.value+1.0f) * 10f);
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0f));
                    float target = 15f * direction * 100f * GigSpeechManager.Instance.parameters.portraitShakeIntensity;
                    portraitSequence.Join(portraitAnchor.DOLocalMoveX(0f, 0.2f).SetEase(Ease.OutQuad).From(target, true));
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0.2f).SetEase(Ease.OutBounce).From());
                    if (_speakerIsRight)
                    {
                        Color c = portraitR.color;
                        c.a = 1f;
                        portraitR.color = c;
                    }
                    else
                    {
                        Color c = portraitL.color;
                        c.a = 1f;
                        portraitL.color = c;
                    }
                }
                else if(!isListener)
                {
                    Vector3 randomRotation = direction * ((Random.value+1.0f) * 10f) * Vector3.forward;
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0f));
                    portraitSequence.AppendInterval(0.4f);
                    portraitSequence.Append(portraitAnchor.DOLocalRotate(randomRotation, 0.2f).SetEase(Ease.OutBounce).From());
                    if (_speakerIsRight)
                    {
                        Color c = portraitR.color;
                        c.a = 1f;
                        portraitR.color = c;
                    }
                    else
                    {
                        Color c = portraitL.color;
                        c.a = 1f;
                        portraitL.color = c;
                    }
                }
            }
        }
    }
}
