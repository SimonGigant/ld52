﻿using System;
using UnityEngine;

namespace GigSpeech
{
    public class GigSpeechDialogueSpawn : MonoBehaviour
    {
        public GigSpeechDialogueSequence gigSpeechDialogueSequence;
        private bool _triggered = false;

        public bool triggerOnStart = true;
        public float delay = 1.0f;
        
        private void Start()
        {
            if(triggerOnStart)
                Invoke(nameof(StartDialogue), delay);
        }

        public void SetBackPlayerCanInteract()
        {
            GameflowManager.Instance.playerCanInteract = true;
        }
        
        public void StartDialogue()
        {
            if (!_triggered)
            {
                GameflowManager.Instance.playerCanInteract = false;
                _triggered = true;
                GameObject dialogue = Instantiate(GigSpeechManager.Instance.parameters.dialoguePrefab);
                GigSpeechDialogueHandler dialogueHandler = dialogue.GetComponentInChildren<GigSpeechDialogueHandler>();
                dialogueHandler.thisGigSpeechDialogue = gigSpeechDialogueSequence;
                if (!triggerOnStart)
                {
                    dialogueHandler.callbackAfterDialogue.AddListener(GameflowManager.Instance.GoToNextLevel);
                }
                else
                {
                    dialogueHandler.callbackAfterDialogue.AddListener(SetBackPlayerCanInteract);
                }
                
            }
        }
    }
}
