using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

[ExecuteInEditMode]
public class EditorGrid : MonoBehaviour {
    public bool active = true;
    private float x, y, z;
    public SokobanGrid grid;
	
    void Start()
    {
        x = 0f;
        y = 0f;
        z = 0f;
        grid = transform.root.GetComponentInChildren<SokobanGrid>();
    }

    void Update () {
        if (!Application.IsPlaying(gameObject) && grid != null)
        {
            x = Mathf.Round((transform.position.x - grid.xOffset) / grid.gridPosMult) * grid.gridPosMult + grid.xOffset;
            y = Mathf.Round((transform.position.y - grid.yOffset) / grid.gridPosMult) * grid.gridPosMult + grid.yOffset;
            z = transform.position.z;
            transform.position = new Vector3(x, y, z);
        }
    }
	
}