using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TransitionAnim : MonoBehaviour
{
    public bool In;

    public RectTransform Object1;
    public RectTransform Object2;
    public RectTransform Object3;

    private void Destr()
    {
        Destroy(gameObject);
    }

    private void Start()
    {
        if (In)
        {
            Object1.DOMoveX(-2000.0f, 1.0f).From().SetEase(Ease.InOutCubic);
            Object2.DOMoveX(-2000.0f, 1.0f).From().SetEase(Ease.InOutCubic);
            Object3.DOMoveX( 7000.0f, 1.2f).From().SetEase(Ease.OutCubic);
        }
        else
        {
            Object1.DOMoveX(-2000.0f, 1.0f).SetEase(Ease.InOutCubic);
            Object2.DOMoveX(-2000.0f, 1.0f).SetEase(Ease.InOutCubic);
            Object3.DOMoveX( 7000.0f, 1.2f).SetEase(Ease.InCubic).OnComplete(Destr);
        }
    }
}
