﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeepManager : MonoBehaviour
{
    public bool isFixed = false;
    public SpriteRenderer _renderer;

    private void Start()
    {
        if (!TryGetComponent<SpriteRenderer>(out _renderer))
            _renderer = GetComponentInChildren<SpriteRenderer>();
        
        _renderer.sortingOrder = Mathf.RoundToInt(transform.position.y * 100.0f);
    }
    
    private void Update()
    {
        if (!isFixed)
        {
            _renderer.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100.0f);
        }
    }
}
