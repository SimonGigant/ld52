using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class RandomSprite : MonoBehaviour
{
    public Sprite[] possibleSprites;

    public bool SineWaveMove = false;
    
    void Start()
    {
        SpriteRenderer renderer = GetComponent<SpriteRenderer>();
        int randVal = Mathf.FloorToInt(Random.Range(0, possibleSprites.Length - 0.01f));
        renderer.sprite = possibleSprites[randVal];

        if (SineWaveMove)
        {
            transform.DOLocalMoveY(0.3f, 3.0f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
        }
    }
}
