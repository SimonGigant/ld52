using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance
    {
        get
        {
            return _instance;
        }
    }
    
    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
    }
    
    private void Start()
    {
        musicSource.Play();
        DontDestroyOnLoad(gameObject);
    }

    public AudioSource musicSource;
    public AudioSource[] voiceSource;
    public AudioClip[] DeathVoice;

    private int currVoice;
    public void PlayVoiceSound()
    {
        AudioClip clip = DeathVoice[Random.Range(0,DeathVoice.Length)];
        //if (!voiceSource[currVoice].isPlaying)
        {
            voiceSource[currVoice].clip = clip;
            voiceSource[currVoice].pitch = 1.5f + 0.3f * Random.value;
            voiceSource[currVoice].Play();
            currVoice = (currVoice + 1) %2;
        }
    }
}
