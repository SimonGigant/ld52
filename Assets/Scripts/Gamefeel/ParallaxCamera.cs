﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxCamera : MonoBehaviour
{
    private static ParallaxCamera _instance;
    public static ParallaxCamera Instance
    {
        get
        {
            return _instance;
        }
    }
    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
    }
    
    
    [SerializeField] public Transform follow = null;
    private Vector2 startPos;
    private Vector2 target;

    private void Start()
    {
        startPos = transform.position;
    }

    private void Update()
    {
        if (follow != null)
        {
            target = Vector2.Lerp(startPos, follow.position, 0.12f);
            Vector3 position = transform.position;
            Vector2 currentPos = Vector2.Lerp(position, target, Time.deltaTime * 4f);
            position = new Vector3(currentPos.x, currentPos.y, position.z);
            transform.position = position;
        }
    }
}
