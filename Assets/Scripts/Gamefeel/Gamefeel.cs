﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gamefeel : MonoBehaviour
{
	
	
	private static Gamefeel _instance;
	public static Gamefeel Instance
	{
		get {
			if (_instance == null)
			{
				_instance = GameObject.FindObjectOfType<Gamefeel>();
				if (_instance == null)
				{
					GameObject container = new GameObject("Gamefeel");
					_instance = container.AddComponent<Gamefeel>();
				}
			}
			return _instance;
		}
	}

	private Camera _cam;
	private GameObject _target;
	private float _startDistance;
	private float _endDistance;
	private float _vertigoDuration;
	private float _vertigoCount;
	private float _startingFOV;
	private AnimationCurve _vertigoCurve;
	private Vector3 _positionChangeVertigo;
	private bool _inVertigo = false;
	
	private float _shakeMagnitude;
	private float _shakeDuration;
	private float _shakeCount;
	private Vector3 _positionChangeShake;
	private AnimationCurve _shakeCurve;
	private bool _inShake = false;

    public AnimationCurve thisShakeCurve;
    public AnimationCurve thisFreezeCurve;

    private float _freezeMagnitude;
    private float _freezeDuration;
    private float _freezeCount;
    private AnimationCurve _freezeCurve;
    private bool _inFreeze = false;
	
	public void InitVertigo(Camera thisCam, GameObject thisTarget, float duration, float force, AnimationCurve curve)
	{
		if (_inVertigo) return;
		_cam = thisCam;
		_target = thisTarget;
		_vertigoDuration = duration;
		_vertigoCurve = curve;
		_startDistance = Vector3.Distance(_cam.transform.position, _target.transform.position);
		_endDistance = _startDistance - force;
		_startingFOV = _cam.fieldOfView;
		_vertigoCount = 0f;
		StartCoroutine(Vertigo());
	}
	
	
    private IEnumerator Vertigo(){
		_inVertigo = true;
		for(;;){
			_vertigoCount+=Time.deltaTime;
			Vector3 direction = (_target.transform.position - _cam.transform.position).normalized;
			float distance = _startDistance + _vertigoCurve.Evaluate(_vertigoCount/_vertigoDuration)*(_endDistance-_startDistance);
			_cam.transform.position = _target.transform.position - distance*direction;
			_positionChangeVertigo += _cam.transform.position;
			//Compute FOV angle and convert from radians to degrees
			float angle = (180 / Mathf.PI) * Mathf.Atan(1 / distance);
			float startAngle = (180/Mathf.PI) * Mathf.Atan(1 / _startDistance);
			_cam.fieldOfView = angle * _startingFOV / startAngle;
			if(_vertigoCount < _vertigoDuration){
				yield return null;
			}else{
				_cam.fieldOfView = _startingFOV;
				_cam.transform.position = _target.transform.position - _cam.transform.forward * _startDistance;
				break;
			}
		}
		_inVertigo = false;
	}

    public void InitScreenshake(float duration, float force)
    {
        InitScreenshake(Camera.main, duration, force, thisShakeCurve);
    }
	
	public void InitScreenshake(Camera thisCam, float duration, float force, AnimationCurve curve)
	{
		if (_inShake) return;
		_cam = thisCam;
		_shakeDuration = duration;
		_shakeMagnitude = force;
		_shakeCount = 0f;
		_shakeCurve = curve;
		_positionChangeShake = new Vector3(0,0,0);
		StartCoroutine(Screenshake());
	}
	
    //Modifié pour adoucir le retour du shake
	private IEnumerator Screenshake(){
		_inShake = true;
		for(;;){
            while (_inFreeze)
            {
                yield return null;
            }
			_shakeCount+=Time.deltaTime;
			Vector3 change = Random.insideUnitSphere * (_shakeCurve.Evaluate(_shakeCount/_shakeDuration) * _shakeMagnitude);
			_cam.transform.localPosition += change - _positionChangeShake;
			_positionChangeShake = change;
			if(_shakeCount < _shakeDuration){
				yield return null;
			}else{
                _cam.transform.localPosition -= _positionChangeShake;
				break;
			}
		}
		_inShake = false;
	}

    public bool IsInFreeze()
    {
        return _inFreeze;
    }

    public void InitFreezeFrame(float duration, float force)
    {
        InitFreezeFrame(duration, force, thisFreezeCurve);
    }

    public void InitFreezeFrame(float duration, float force, AnimationCurve curve)
    {
	    if (_inFreeze) return;
	    _freezeDuration = duration;
	    _freezeMagnitude = force;
	    _freezeCurve = curve;
	    StartCoroutine(FreezeFrame());
    }

    private void Start()
    {
        Time.timeScale = 1.0f;
    }

    private IEnumerator FreezeFrame()
    {
        _inFreeze = true;
        float original = Time.timeScale;
        _freezeCount = 0f;
        for(;;)
        {
            _freezeCount += Time.unscaledDeltaTime;
            float freezeValue = _freezeCurve.Evaluate(_freezeCount / _freezeDuration) * _freezeMagnitude;
            Time.timeScale = 0f;
            if (_freezeCount < _freezeDuration) {
                yield return new WaitForSecondsRealtime(freezeValue);
                Time.timeScale = original;
            }
            else
            {
                Time.timeScale = original;
                break;
            }
        }
        _inFreeze = false;
    }
}
