﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VFXSpawned : MonoBehaviour
{
    public float duration = 5f;
    public bool isSpritesheet;

    private SimpleAnimation _anim;
    void Start()
    {
        if (isSpritesheet)
        {
            _anim = GetComponent<SimpleAnimation>();
            StartCoroutine(WaitBeforeDestroy());
        }
        else
            StartCoroutine(Despawn());
    }

    private IEnumerator Despawn()
    {
        yield return new WaitForSeconds(duration);
        Destroy(gameObject);
    }
    private IEnumerator WaitBeforeDestroy()
    {
        yield return new WaitForSeconds(_anim.animator.GetCurrentAnimatorStateInfo(0).length);
        Destroy(gameObject);
    }
}
