using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulBagEntity : SokobanEntity
{
    public override bool CanTakeSoul()
    {
        return true;
    }

    public override void OnTakeSoul(SoulEntity soul)
    {
        SokobanGrid.Instance.DestroyEntity(soul);
        GameflowManager.Instance.HarvestSoul();
        if (VFXOnTakeSoul != null)
        {
            Instantiate(VFXOnTakeSoul, transform.position,transform.rotation);
        }
    }

    public GameObject VFXOnTakeSoul;
}
