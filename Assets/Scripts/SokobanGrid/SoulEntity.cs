using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulEntity : SokobanEntity
{
    public Sprite leftArrow;
    public Sprite rightArrow;
    public Sprite upArrow;
    public Sprite downArrow;

    public SpriteRenderer arrowHelper;
    
    private void Start()
    {
        ++GameflowManager.Instance.soulToHarvest;
    }

    private void FixedUpdate()
    {
        Tuple<int, int> pos = GetPosition();
        Tuple<int, int> leftPos = SokobanGrid.AddPositions(pos, new Tuple<int, int>(-1, 0));
        Tuple<int, int> rightPos = SokobanGrid.AddPositions(pos, new Tuple<int, int>(1, 0));
        Tuple<int, int> upPos = SokobanGrid.AddPositions(pos, new Tuple<int, int>(0, 1));
        Tuple<int, int> downPos = SokobanGrid.AddPositions(pos, new Tuple<int, int>(0, -1));

        SokobanEntity leftEntity = SokobanGrid.Instance.GetEntityAtPos(leftPos);
        SokobanEntity rightEntity = SokobanGrid.Instance.GetEntityAtPos(rightPos);
        SokobanEntity upEntity = SokobanGrid.Instance.GetEntityAtPos(upPos);
        SokobanEntity downEntity = SokobanGrid.Instance.GetEntityAtPos(downPos);
        if (leftEntity != null && leftEntity.IsPlayer() && (rightEntity != null && !rightEntity.IsPushable() && !rightEntity.CanTakeSoul() || !SokobanGrid.Instance.IsPositionValid(rightPos)))
        {
            arrowHelper.enabled = true;
            arrowHelper.sprite = upArrow;
        }
        else if (rightEntity != null && rightEntity.IsPlayer() && (leftEntity != null && !leftEntity.IsPushable() && !leftEntity.CanTakeSoul() || !SokobanGrid.Instance.IsPositionValid(leftPos)))
        {
            arrowHelper.enabled = true;
            arrowHelper.sprite = downArrow;
        }
        else if(upEntity != null && upEntity.IsPlayer() && ( downEntity != null && !downEntity.IsPushable() && !downEntity.CanTakeSoul()|| !SokobanGrid.Instance.IsPositionValid(downPos)))
        {
            arrowHelper.enabled = true;
            arrowHelper.sprite = rightArrow;
        }else if (downEntity != null && downEntity.IsPlayer() && (upEntity != null && !upEntity.IsPushable() && !upEntity.CanTakeSoul() || !SokobanGrid.Instance.IsPositionValid(upPos)))
        {
            arrowHelper.enabled = true;
            arrowHelper.sprite = leftArrow;
        }
        else
        {
            arrowHelper.enabled = false;
        }
    }

    public override bool IsPushable()
    {
        return true;
    }

    public override bool TryPush(SokobanEntity from)
    {
        Tuple<int, int> posFrom = from.GetPosition();
        Tuple<int, int> thisPos = GetPosition();
        Tuple<int, int> direction = SokobanGrid.SubstractPosition(thisPos, posFrom);
        Tuple<int, int> nextPos = SokobanGrid.AddPositions(thisPos, direction);
        SokobanEntity entity = SokobanGrid.Instance.GetEntityAtPos(nextPos);
        if (entity != null && entity.CanTakeSoul())
        {
            entity.OnTakeSoul(this);
            from.TryMove(direction);
            return true;
        }
        
        if (TryMove(direction))
        {
            from.TryMove(direction);
            return true;
        }
        else
        {
            Tuple<int, int> newDirection = new Tuple<int, int>(0, 0);
            if (direction.Item1 == 0)
            {
                if (direction.Item2 == 1)
                    newDirection = new Tuple<int, int>(-1, 0);
                if (direction.Item2 == -1)
                    newDirection = new Tuple<int, int>(1, 0);
            }
            else
            {
                if (direction.Item1 == 1)
                    newDirection = new Tuple<int, int>(0, 1);
                if (direction.Item1 == -1)
                    newDirection = new Tuple<int, int>(0, -1);
            }
            Tuple<int, int> newPos = SokobanGrid.AddPositions(thisPos, newDirection);
            SokobanEntity newEntity = SokobanGrid.Instance.GetEntityAtPos(newPos);
            
            if (newEntity != null && newEntity.CanTakeSoul())
            {
                newEntity.OnTakeSoul(this);
                from.TryMove(direction);
                return true;
            }

            if (TryMove(newDirection))
            {
                from.TryMove(direction);
                return true;
            }
        }

        return false;
    }
}
