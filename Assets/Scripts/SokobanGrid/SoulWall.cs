using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoulWall : SokobanEntity
{
    public override bool CanTakeSoul()
    {
        return true;
    }

    public override void OnTakeSoul(SoulEntity soul)
    {
        SokobanGrid.Instance.DestroyEntity(this);
        Gamefeel.Instance.InitScreenshake(0.3f,0.2f);
    }
}
