using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EntitySpawner : MonoBehaviour
{
    public GameObject entityPrefab;

    private void Start()
    {
        NotifyToGridManager();
    }

    public void NotifyToGridManager()
    {
        Tuple<int, int> position = SokobanGrid.Instance.WorldPositionToGridPosition(transform.position);
        SokobanGrid.Instance.CreateEntity(entityPrefab, position);
        Destroy(gameObject);
    }
}
