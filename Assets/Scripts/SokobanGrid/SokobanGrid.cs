using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEditor;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;


public class SokobanEntity : MonoBehaviour
{

    public bool duringMovement = false;
    public UnityEvent onMovement;

    public virtual bool IsPlayer()
    {
        return false;
    }
    
    public virtual bool IsPushable()
    {
        return false;
    }

    public virtual bool CanTakeSoul()
    {
        return false;
    }

    public virtual void OnTakeSoul(SoulEntity soul)
    {
        
    }
    
    public Tuple<int, int> GetPosition()
    {
        return SokobanGrid.Instance.GetEntityPosition(this);
    }

    protected virtual void OnMovementComplete()
    {
        duringMovement = false;
    }

    public virtual bool TryPush(SokobanEntity from)
    {
        return false;
    }
    

    public void MoveTo(Tuple<int, int> position)
    {
        SokobanGrid.Instance.MoveEntityTo(this, position);
        duringMovement = true;
        transform.DOMove(SokobanGrid.Instance.GridPositionToWorldPosition(position),0.2f).SetEase(Ease.InOutFlash).OnComplete(OnMovementComplete);
        onMovement.Invoke();
    }

    public bool TryMove(Tuple<int, int> direction)
    {
        if (duringMovement)
            return false;
        Tuple<int, int> currPos = GetPosition();
        Tuple<int, int> nextPos = SokobanGrid.AddPositions(currPos, direction);
        if (SokobanGrid.Instance.IsPositionValid(nextPos))
        {
            SokobanEntity entityNextPos = SokobanGrid.Instance.GetEntityAtPos(nextPos);
            if (entityNextPos != null && entityNextPos.IsPushable())
            {
                if (!entityNextPos.TryPush(this))
                {
                    return false;
                }
                return true;
            }
            else if(entityNextPos == null)
            {
                MoveTo(nextPos);
                return true;
            }
        }
        return false;
    }
}

public class SokobanGrid : MonoBehaviour
{
    private static SokobanGrid _instance;
    public static SokobanGrid Instance
    {
        get
        {
            return _instance;
        }
    }

    public float xOffset = 0.0f;
    public float yOffset = 0.0f;
    public float gridPosMult = 5.0f;
    public GameObject WallEntity;

    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;
        
        InitiateEntities();
    }

    private void InitiateEntities()
    {
        entities = new SokobanEntity[rowSize, columnSize];
        for (int i = 0; i < rowSize; ++i)
        {
            for (int j = 0; j < columnSize; ++j)
            {
                if (Physics2D.OverlapBox(GridPositionToWorldPosition(new Tuple<int, int>(i,j)), Vector2.one * 0.5f, 0.0f))
                {
                    CreateEntity(WallEntity, new Tuple<int, int>(i,j));
                }
            }
        }
        
    }

    public void CreateEntity(GameObject entityPrefab, Tuple<int, int> gridPos)
    {
        if (IsPositionValid(gridPos) && GetEntityAtPos(gridPos) == null)
        {
            Vector3 worldPosition = GridPositionToWorldPosition(gridPos);
            GameObject instantiatedObject = GameObject.Instantiate(entityPrefab, worldPosition,Quaternion.identity, transform);
            SokobanEntity entity = instantiatedObject.GetComponent<SokobanEntity>();
            if (entity == null)
            {
                Debug.LogError("spawned entity does not have entity component");
                return;
            }
            SetEntityAtPos(entity, gridPos);
        }
    }

    public static Tuple<int, int> AddPositions(Tuple<int, int> a, Tuple<int, int> b)
    {
        int c1 = a.Item1 + b.Item1;
        int c2 = a.Item2 + b.Item2;
        return new Tuple<int, int>(c1, c2);
    }
    
    public static Tuple<int, int> SubstractPosition(Tuple<int, int> a, Tuple<int, int> b)
    {
        int c1 = a.Item1 - b.Item1;
        int c2 = a.Item2 - b.Item2;
        return new Tuple<int, int>(c1, c2);
    }

    public Tuple<int, int> GetEntityPosition(SokobanEntity entity)
    {
        for (int i = 0; i < rowSize; ++i)
        {
            for (int j = 0; j < columnSize; ++j)
            {
                if (entities[i, j] == entity)
                {
                    return new Tuple<int, int>(i, j);
                }
            }
        }
        Debug.LogError("Cannot find entity's position");
        return new Tuple<int, int>(-1, -1);
    }

    public SokobanEntity GetEntityAtPos(Tuple<int, int> pos)
    {
        if(IsPositionValid(pos))
            return entities[pos.Item1, pos.Item2];
        return null;
    }

    public void SetEntityAtPos(SokobanEntity entity, Tuple<int, int> pos)
    {
        entities[pos.Item1, pos.Item2] = entity;
        if(entity != null)
            entity.transform.position = GridPositionToWorldPosition(pos); //Temporary ?
    }

    public void DestroyEntity(SokobanEntity entity)
    {
        Tuple<int, int> pos = GetEntityPosition(entity);
        SetEntityAtPos(null, pos);
        Destroy(entity.gameObject);
    }

    public bool MoveEntityTo(SokobanEntity entity, Tuple<int, int> newPos)
    {
       Tuple<int,int> oldPos = GetEntityPosition(entity);
       if (entities[newPos.Item1, newPos.Item2] != null)
       {
           Debug.Log("Cannot move entity to " + newPos.Item1 + "," + newPos.Item2 + " : there is already an entity");
           return false;
       }
       entities[newPos.Item1, newPos.Item2] = entity;
       entities[oldPos.Item1, oldPos.Item2] = null;
       return true;
    }

    public bool IsPositionValid(Tuple<int, int> pos)
    {
        return pos.Item1 >= 0 && pos.Item2 >= 0 && pos.Item1 < rowSize && pos.Item2 < columnSize;
    }

    public Vector2 GridPositionToWorldPosition(Tuple<int, int> gridPosition)
    {
        float xVal = gridPosition.Item1 * gridPosMult + xOffset;
        float yVal = gridPosition.Item2 * gridPosMult + yOffset;
        return new Vector2(xVal, yVal);
    }

    public Tuple<int, int> WorldPositionToGridPosition(Vector2 worldPosition)
    {
        int xVal = Mathf.RoundToInt((worldPosition.x - xOffset) / gridPosMult);
        int yVal = Mathf.RoundToInt((worldPosition.y - yOffset) / gridPosMult);
        return new Tuple<int, int>(xVal, yVal);
    }

    public int rowSize = 8;
    public int columnSize = 6;

    public SokobanEntity[,] entities;

    public void PrintGridDebug()
    {
        Vector2 offset = Vector2.one * (gridPosMult / 2);
        for (int i = 0; i <= rowSize; ++i)
        {
            Vector3 from = GridPositionToWorldPosition(new Tuple<int, int>(i,0))-offset;
            Vector3 to = GridPositionToWorldPosition(new Tuple<int, int>(i, columnSize))-offset;
            //Handles.DrawLine(from, to );
            Debug.DrawLine(from, to, Color.green, 1000, false);
        }
        
        for (int j = 0; j <= columnSize; ++j)
        {
            Vector3 from = GridPositionToWorldPosition(new Tuple<int, int>(0, j)) - offset;
            Vector3 to = GridPositionToWorldPosition(new Tuple<int, int>(rowSize, j)) - offset;
            //Handles.DrawLine(from, to );
            Debug.DrawLine(from, to, Color.green, 1000, false);
        }
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(SokobanGrid))]
class SokobanGridEditor : Editor
{
    private void OnSceneGUI()
    {
        SokobanGrid grid = target as SokobanGrid;
        if (grid == null)
            return;
        grid.PrintGridDebug();
    }
}
#endif
