using System;
using System.Collections;
using System.Collections.Generic;
using GigSpeech;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameflowManager : MonoBehaviour
{
    private static GameflowManager _instance;
    public static GameflowManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public GameObject TransitionIn;
    public GameObject TransitionOut;
    
    private void Awake()
    {
        // if the singleton hasn't been initialized yet
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }

        _instance = this;
    }

    private void Start()
    {
        GameObject go = Instantiate(TransitionOut);
    }

    public int currentSoulHarvested = 0;
    public int soulToHarvest = 0;
    public bool playerCanInteract = true;

    public void HarvestSoul()
    {
        ++currentSoulHarvested;
        if (currentSoulHarvested >= soulToHarvest)
        {
            Gamefeel.Instance.InitScreenshake(0.3f,0.6f);
            PlayDialogue();
        }
        else
        {
            Gamefeel.Instance.InitScreenshake(0.3f,0.2f);
        }
    }

    public void PlayDialogue()
    {
        GigSpeechDialogueSpawn spawner = GetComponent<GigSpeechDialogueSpawn>();
        if (spawner == null || spawner.gigSpeechDialogueSequence == null)
        {
            GoToNextLevel();
        }
        else
        {
            spawner.StartDialogue();
        }
    }

    public void GoToNextLevel()
    {
        GameObject go = Instantiate(TransitionIn);
        Invoke(nameof(OnEndTransitionNextLevel),2f);
    }

    private void OnEndTransitionNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }

    private void OnEndTransitionReloadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void ReloadLevel()
    {
        GameObject go = Instantiate(TransitionIn);
        Invoke(nameof(OnEndTransitionReloadLevel),2f);
    }
}
